# Service Discovery
This project is a Service Discovery for the Rent Car application
It uses the Netflix Eureka. 

## Requirements
* java 11
* Maven

## Running the application
1. Clone the repo
2. Run the command:
> mvn spring-boot:run

